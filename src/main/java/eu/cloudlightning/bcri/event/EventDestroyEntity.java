package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

public class EventDestroyEntity {
    private String entityId;
    private String eventType;
    private String serverId;
    private long timestamp;
    private String reason;
    List<String> targets;

    public EventDestroyEntity() {
	this.entityId = "";
	this.serverId = "";
	this.timestamp = 0;
	this.reason = "";
	this.eventType = EventType.EVENT_DESTROY_ENTITY.name();
	this.targets = new ArrayList<String>();
    }

    public EventDestroyEntity(String entityId) {
	this.entityId = entityId;
	this.serverId = "";
	this.timestamp = 0;
	this.reason = "";
	this.eventType = EventType.EVENT_DESTROY_ENTITY.name();
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the serverId
     */
    public String getServerId() {
	return serverId;
    }

    /**
     * @param serverId
     *            the serverId to set
     */
    public void setServerId(String serverId) {
	this.serverId = serverId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the reason
     */
    public String getReason() {
	return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(String reason) {
	this.reason = reason;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }

}
