/*
 * Copyright 2015-17 (C) <BCRI, University College Cork>
 * 
 * Author     : Dr. Dapeng Dong
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * VERSION     AUTHOR/      DESCRIPTION OF CHANGE
 * OLD/NEW     DATE                RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0  | Dr. Dapeng Dong   | Initial Create.
 *---------|-------------------|----------------------------------------------
 */
package eu.cloudlightning.bcri.event;

public enum EventType {
	EVENT_SOSM_CONFIGURATION("SOSM configuration"), EVENT_METRIC("metric"), EVENT_KEEPALIVE("keepalive"), EVENT_WEIGHT(
			"weight"), EVENT_BP_RESOURCE_SERVICE_ELEMENT("resource_request"), EVENT_RESOURCE_REGISTRATION(
			"resource_registration"), EVENT_RESOURCE_DEREGISTRATION(
			"resource_deregistration"), EVENT_RESOURCE_DECOMMISSION(
			"resource_decommission"), EVENT_DESTROY_ENTITY(
			"destroy_entity"), EVENT_RESOURCE_CAPACITY(
			"resource_capacity"), EVENT_BP_RESOURCE_TEMPLATE(
			"receive_blueprint_resource_template"), EVENT_BP_RESOURCED_SERVICE_ELEMENT(
			"receive_blueprint_resourced_service_element"), EVENT_REQUEST_CONFIGURATION(
			"request for configuration information"), EVENT_RESOURCE_REGISTRATION_RESPONSE(
			"resource reqistration response"), EVENT_RESOURCE_DEREGISTRATION_RESPONSE(
			"resource de-registration response"), EVENT_PROUTER_LIST("updated pRouter list");
	private String eventType;

	EventType(String eventType) {
		this.eventType = eventType;
	}

	@Override
	public String toString() {
		return eventType;
	}
}