/*
 * This class is a rest server basement
 */
package eu.cloudlightning.restserver;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.util.Optional;

/**
 *
 * @author gabriel
 */
public class RestServer {
     // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI;
    public static final Optional<String> protocol;
    public static final Optional<String> host;
    public static final Optional<String> path;
    public static final Optional<String> port;
    public static final String pointedPackage;
    
    static{
      protocol = Optional.ofNullable(System.getenv("REST_PROTOCOL"));
      host = Optional.ofNullable(System.getenv("REST_HOSTNAME"));
      port = Optional.ofNullable(System.getenv("REST_PORT"));
      path = Optional.ofNullable(System.getenv("REST_PATH"));
      pointedPackage = System.getenv("REST_PACKAGE");
      BASE_URI = protocol.orElse("http://") + host.orElse("localhost") + ":" + port.orElse("8080") + "/" + path.orElse("/") + "/";
    }
    
    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {

        // create a resource config that scans for JAX-RS resources and providers
        final ResourceConfig rc = new ResourceConfig().packages(pointedPackage);

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        System.out.println(pointedPackage.toString());
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }
}