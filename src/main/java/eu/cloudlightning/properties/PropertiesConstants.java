/*
 * Set of DB table names (or collections).
 */
package eu.cloudlightning.properties;

/**
 *
 * @author Gabriel Gonzalez
 */
 public class PropertiesConstants {

    public static final String CONFIGURATION = "config.properties";
    public static final String DBHOST_KEY = "dbhost";
    public static final String DBNAME_KEY = "dbname";
    public static final String DBPORT_KEY = "dbport";
    public static final String DBPROTOCOL_KEY = "dbprotocol";
    public static final String DBUSERAUTH_KEY = "dbuser";
    public static final String DBPASSWORDAUTH_KEY = "dbpassword";
    public static final String DBURL_KEY = "dburl";
    
    public static String DBHOST_VALUE;
    public static String DBNAME_VALUE;
    public static String DBPORT_VALUE;
    public static String DBPROTOCOL_VALUE;
    public static String DBUSERAUTH_VALUE;    
    public static String DBPASSWORDAUTH_VALUE;
    public static String DBURL_VALUE;
    
    public static final String REQUEST_TIMEOUT_KEY = "REQUEST_TIMEOUT";
    public static final String REST_HOSTNAME_KEY = "REST_HOSTNAME";
    public static final String REST_PORT_KEY = "REST_PORT";
    public static final String REST_PACKAGE_KEY = "REST_PACKAGE";
    public static final String REST_PATH_KEY = "REST_PATH";
    public static final String REST_PROTOCOL_KEY = "REST_PROTOCOL";
    public static String REQUEST_TIMEOUT_VALUE;
    public static String REST_HOSTNAME_VALUE;
    public static String REST_PORT_VALUE;
    public static String REST_PACKAGE_VALUE;
    public static String REST_PATH_VALUE;
    public static String REST_PROTOCOL_VALUE;

    public static final String RABBITMQ_HOST_KEY_RECEPTION = "RABBITMQ_HOST_RECEPTION";
    public static final String RABBITMQ_UID_KEY_RECEPTION = "RABBITMQ_UID_RECEPTION";
    public static final String RABBITMQ_PWD_KEY_RECEPTION = "RABBITMQ_PWD_RECEPTION";
    public static final String RABBITMQ_QNAME_KEY_RECEPTION = "RABBITMQ_QNAME_RECEPTION";    
    public static String RABBITMQ_HOST_VALUE_RECEPTION;
    public static String RABBITMQ_UID_VALUE_RECEPTION;
    public static String RABBITMQ_PWD_VALUE_RECEPTION;       
    public static String RABBITMQ_QNAME_VALUE_RECEPTION;  
    
    public static final String RABBITMQ_HOST_KEY_FORWARD = "RABBITMQ_HOST_FORWARD";
    public static final String RABBITMQ_UID_KEY_FORWARD = "RABBITMQ_UID_FORWARD";
    public static final String RABBITMQ_PWD_KEY_FORWARD = "RABBITMQ_PWD_FORWARD";
    public static final String RABBITMQ_QNAME_KEY_FORWARD = "RABBITMQ_QNAME_FORWARD";    
    public static String RABBITMQ_HOST_VALUE_FORWARD;
    public static String RABBITMQ_UID_VALUE_FORWARD;
    public static String RABBITMQ_PWD_VALUE_FORWARD;       
    public static String RABBITMQ_QNAME_VALUE_FORWARD;  
    
    public static final String COMPONENTID_KEY = "component_id";
    public static String COMPONENTID_VALUE;

    public static String getDBHOST_VALUE() {
        return DBHOST_VALUE;
    }

    public static void setDBHOST_VALUE(String DBHOST_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.DBHOST_VALUE = DBHOST_VALUE;
    }

    public static String getDBNAME_VALUE() {
        return DBNAME_VALUE;
    }

    public static void setDBNAME_VALUE(String DBNAME_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.DBNAME_VALUE = DBNAME_VALUE;
    }

    public static String getDBPORT_VALUE() {
        return DBPORT_VALUE;
    }

    public static void setDBPORT_VALUE(String DBPORT_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.DBPORT_VALUE = DBPORT_VALUE;
    }

    public static String getDBPROTOCOL_VALUE() {
        return DBPROTOCOL_VALUE;
    }

    public static void setDBPROTOCOL_VALUE(String DBPROTOCOL_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.DBPROTOCOL_VALUE = DBPROTOCOL_VALUE;
    }

    public static String getDBUSERAUTH_VALUE() {
        return DBUSERAUTH_VALUE;
    }

    public static void setDBUSERAUTH_VALUE(String DBUSERAUTH_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.DBUSERAUTH_VALUE = DBUSERAUTH_VALUE;
    }

    public static String getDBPASSWORDAUTH_VALUE() {
        return DBPASSWORDAUTH_VALUE;
    }

    public static void setDBPASSWORDAUTH_VALUE(String DBPASSWORDAUTH_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.DBPASSWORDAUTH_VALUE = DBPASSWORDAUTH_VALUE;
    }

    public static String getREQUEST_TIMEOUT_VALUE() {
        return REQUEST_TIMEOUT_VALUE;
    }

    public static void setREQUEST_TIMEOUT_VALUE(String REQUEST_TIMEOUT_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.REQUEST_TIMEOUT_VALUE = REQUEST_TIMEOUT_VALUE;
    }

    public static String getREST_HOSTNAME_VALUE() {
        return REST_HOSTNAME_VALUE;
    }

    public static void setREST_HOSTNAME_VALUE(String REST_HOSTNAME_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.REST_HOSTNAME_VALUE = REST_HOSTNAME_VALUE;
    }

    public static String getREST_PORT_VALUE() {
        return REST_PORT_VALUE;
    }

    public static void setREST_PORT_VALUE(String REST_PORT_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.REST_PORT_VALUE = REST_PORT_VALUE;
    }

    public static String getREST_PACKAGE_VALUE() {
        return REST_PACKAGE_VALUE;
    }

    public static void setREST_PACKAGE_VALUE(String REST_PACKAGE_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.REST_PACKAGE_VALUE = REST_PACKAGE_VALUE;
    }

    public static String getREST_PATH_VALUE() {
        return REST_PATH_VALUE;
    }

    public static void setREST_PATH_VALUE(String REST_PATH_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.REST_PATH_VALUE = REST_PATH_VALUE;
    }

    public static String getREST_PROTOCOL_VALUE() {
        return REST_PROTOCOL_VALUE;
    }

    public static void setREST_PROTOCOL_VALUE(String REST_PROTOCOL_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.REST_PROTOCOL_VALUE = REST_PROTOCOL_VALUE;
    }

    public static String getCOMPONENTID_VALUE() {
        return COMPONENTID_VALUE;
    }

    public static void setCOMPONENTID_VALUE(String COMPONENTID_VALUE) {
        eu.cloudlightning.properties.PropertiesConstants.COMPONENTID_VALUE = COMPONENTID_VALUE;
    }

    public static String getRABBITMQ_HOST_VALUE_RECEPTION() {
        return RABBITMQ_HOST_VALUE_RECEPTION;
    }

    public static void setRABBITMQ_HOST_VALUE_RECEPTION(String RABBITMQ_HOST_VALUE_RECEPTION) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_HOST_VALUE_RECEPTION = RABBITMQ_HOST_VALUE_RECEPTION;
    }

    public static String getRABBITMQ_UID_VALUE_RECEPTION() {
        return RABBITMQ_UID_VALUE_RECEPTION;
    }

    public static void setRABBITMQ_UID_VALUE_RECEPTION(String RABBITMQ_UID_VALUE_RECEPTION) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_UID_VALUE_RECEPTION = RABBITMQ_UID_VALUE_RECEPTION;
    }

    public static String getRABBITMQ_PWD_VALUE_RECEPTION() {
        return RABBITMQ_PWD_VALUE_RECEPTION;
    }

    public static void setRABBITMQ_PWD_VALUE_RECEPTION(String RABBITMQ_PWD_VALUE_RECEPTION) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_PWD_VALUE_RECEPTION = RABBITMQ_PWD_VALUE_RECEPTION;
    }

    public static String getRABBITMQ_QNAME_VALUE_RECEPTION() {
        return RABBITMQ_QNAME_VALUE_RECEPTION;
    }

    public static void setRABBITMQ_QNAME_VALUE_RECEPTION(String RABBITMQ_QNAME_VALUE_RECEPTION) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_QNAME_VALUE_RECEPTION = RABBITMQ_QNAME_VALUE_RECEPTION;
    }

    public static String getRABBITMQ_HOST_VALUE_FORWARD() {
        return RABBITMQ_HOST_VALUE_FORWARD;
    }

    public static void setRABBITMQ_HOST_VALUE_FORWARD(String RABBITMQ_HOST_VALUE_FORWARD) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_HOST_VALUE_FORWARD = RABBITMQ_HOST_VALUE_FORWARD;
    }

    public static String getRABBITMQ_UID_VALUE_FORWARD() {
        return RABBITMQ_UID_VALUE_FORWARD;
    }

    public static void setRABBITMQ_UID_VALUE_FORWARD(String RABBITMQ_UID_VALUE_FORWARD) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_UID_VALUE_FORWARD = RABBITMQ_UID_VALUE_FORWARD;
    }

    public static String getRABBITMQ_PWD_VALUE_FORWARD() {
        return RABBITMQ_PWD_VALUE_FORWARD;
    }

    public static void setRABBITMQ_PWD_VALUE_FORWARD(String RABBITMQ_PWD_VALUE_FORWARD) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_PWD_VALUE_FORWARD = RABBITMQ_PWD_VALUE_FORWARD;
    }

    public static String getRABBITMQ_QNAME_VALUE_FORWARD() {
        return RABBITMQ_QNAME_VALUE_FORWARD;
    }

    public static void setRABBITMQ_QNAME_VALUE_FORWARD(String RABBITMQ_QNAME_VALUE_FORWARD) {
        eu.cloudlightning.properties.PropertiesConstants.RABBITMQ_QNAME_VALUE_FORWARD = RABBITMQ_QNAME_VALUE_FORWARD;
    }

    
    
    
}