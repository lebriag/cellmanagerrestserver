/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cloudlightning.requestchecker;

import eu.cloudlightning.bcri.blueprint.BlueprintResourceDecommissionTemplate;
import eu.cloudlightning.bcri.blueprint.BlueprintResourceTemplate;
import eu.cloudlightning.bcri.event.EventBPResourceTemplate;
import eu.cloudlightning.bcri.event.EventResourceDecommission;
import eu.cloudlightning.bcri.event.EventType;

/**
 *
 * @author gabriel
 */
public class GWRequestParser {
    
    public GWRequestParser (){
        
    }
    
    public EventBPResourceTemplate parseResourceTemplate(BlueprintResourceTemplate registrationDescription, String componentID){
        EventBPResourceTemplate event = new EventBPResourceTemplate();
        
        long timestamp = System.currentTimeMillis();
        event.setEntityId(componentID);
//        event.setEventType(EventType.EVENT_BP_RESOURCE_TEMPLATE.toString());
        event.setTimestamp(timestamp);
        
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        // set specific fields from BP resource template
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
        try{
        
            event.setBlueprintId(registrationDescription.getBlueprintId());
            event.setCost(registrationDescription.getCost());
            event.setCallbackEndpoint(registrationDescription.getCallbackEndpoint());
            event.setServiceElements(registrationDescription.getServiceElements());

        }catch (Exception e){
                 event = null;
        }   
        return event;
    }
    
        
    public EventResourceDecommission parseResourceDecommission(BlueprintResourceDecommissionTemplate bpDecommissionTemplate, String componentID){
        EventResourceDecommission event = new EventResourceDecommission();
        
        long timestamp = System.currentTimeMillis();
        event.setEntityId(componentID);
//        event.setEventType(EventType.EVENT_RESOURCE_DECOMMISSION.toString());
        event.setTimestamp(timestamp);
        
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        // set specific fields from BP resource decommission
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        try{
            event.setBlueprintId(bpDecommissionTemplate.getBlueprintId());
            event.setResourcedServiceElements(bpDecommissionTemplate.getResourcedServiceElements());
            event.setTargets(null);
        }catch (Exception e){
                 event = null;
        }   
        return event;
    }
}
