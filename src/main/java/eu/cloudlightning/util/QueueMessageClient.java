/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cloudlightning.util;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import eu.cloudlightning.properties.PropertiesConstants;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author gabriel
 */
public class QueueMessageClient {

    Consumer consumer;
    private Connection connection;
    private String RABBITMQ_HOST_RECEPTION;
    private String RABBITMQ_UID_RECEPTION;
    private String RABBITMQ_PWD_RECEPTION;
    private String RABBITMQ_QNAME_RECEPTION;
    private String REQUEST_TIMEOUT;
    private Boolean success;
    private Properties config;

    public QueueMessageClient(Properties config)  {
        
        this.RABBITMQ_HOST_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_RECEPTION));
        this.RABBITMQ_UID_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_UID_KEY_RECEPTION));
        this.RABBITMQ_PWD_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_PWD_KEY_RECEPTION));
        this.RABBITMQ_QNAME_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_QNAME_KEY_RECEPTION));
        this.REQUEST_TIMEOUT = config.getProperty(PropertiesConstants.REQUEST_TIMEOUT_KEY);
        this.config = config;             
    }
        
    public Boolean connectToQueue() throws IOException, TimeoutException{
        int timer = 30; //30seconds
        long currentTime = System.currentTimeMillis();
        success = false;
        Boolean exception = false;
        while (( (System.currentTimeMillis() / 1000) - (currentTime / 1000) ) <= timer) {
            try {
                exception = false;
                
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost(RABBITMQ_HOST_RECEPTION);
                factory.setUsername(RABBITMQ_UID_RECEPTION);
                factory.setPassword(RABBITMQ_PWD_RECEPTION);

//                factory.setRequestedHeartbeat(Integer.valueOf(REQUEST_TIMEOUT));
                connection = factory.newConnection();               
                success = true;
            } catch (Exception e) {
                exception = true;
                e.printStackTrace();
                System.out.println("Error");
                
            }
            
            if(connection != null) {
              if (!exception) success = true;
              break;              
            }
        }
        
        return success;
    }
    
    public Channel createChannel (){
    	int timer = 30; //30seconds
        long currentTime = System.currentTimeMillis();
        success = false;
        Channel channel = null;
        Boolean exception = false;
        while (( (System.currentTimeMillis() / 1000) - (currentTime / 1000) ) <= timer) {
            try {
                exception = false;
               
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost(RABBITMQ_HOST_RECEPTION);
                factory.setUsername(RABBITMQ_UID_RECEPTION);
                factory.setPassword(RABBITMQ_PWD_RECEPTION);

//                factory.setRequestedHeartbeat(Integer.valueOf(REQUEST_TIMEOUT));
                channel = this.connection.createChannel();
                
//                channel.exchangeDeclare(RABBITMQ_QNAME_RECEPTION, RABBITMQ_QNAME_RECEPTION);
//                String queueName = channel.queueDeclare().getQueue();
//                channel.queueBind(queueName, RABBITMQ_QNAME_RECEPTION, "");
                channel.queueDeclarePassive(RABBITMQ_QNAME_RECEPTION);
                //channel.basicQos(1);                
                success = true;
            } catch (Exception e) {
                exception = true;
                e.printStackTrace();
                System.out.println("Error");
                
            }
            
            if(channel != null) {
              if (!exception) success = true;
              break;              
            }
        }
        
        return channel;
    	
    }
    
    public String getRABBITMQ_QNAME_RECEPTION(){
        return RABBITMQ_QNAME_RECEPTION;
    }
}

/*
*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
package eu.cloudlightning.util;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import eu.cloudlightning.properties.PropertiesConstants;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;


public class QueueMessageClient {

    Consumer consumer;
    private Channel channel;
    private Connection connection;
    private String RABBITMQ_HOST_RECEPTION;
    private String RABBITMQ_UID_RECEPTION;
    private String RABBITMQ_PWD_RECEPTION;
    private String RABBITMQ_QNAME_RECEPTION;
    private String REQUEST_TIMEOUT;
    private Boolean success;
    private Properties config;

    public QueueMessageClient(Properties config)  {
        
        this.RABBITMQ_HOST_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_RECEPTION));
        this.RABBITMQ_UID_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_UID_KEY_RECEPTION));
        this.RABBITMQ_PWD_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_PWD_KEY_RECEPTION));
        this.RABBITMQ_QNAME_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_QNAME_KEY_RECEPTION));
        this.REQUEST_TIMEOUT = config.getProperty(PropertiesConstants.REQUEST_TIMEOUT_KEY);
        this.config = config;             
    }
    
    
    public Channel getChannel(){
        return channel;
    }
    
    public Boolean connectToQueue() throws IOException, TimeoutException{
        int timer = 30; //30seconds
        this.consumer = new DefaultConsumer(channel);
        long currentTime = System.currentTimeMillis();
        success = false;
        Boolean exception = false;
        while (( (System.currentTimeMillis() / 1000) - (currentTime / 1000) ) <= timer) {
            try {
                exception = false;
                RABBITMQ_HOST_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_HOST_KEY_RECEPTION));
                RABBITMQ_UID_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_UID_KEY_RECEPTION));
                RABBITMQ_PWD_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_PWD_KEY_RECEPTION));
                RABBITMQ_QNAME_RECEPTION = (config.getProperty(PropertiesConstants.RABBITMQ_QNAME_KEY_RECEPTION));
                REQUEST_TIMEOUT = config.getProperty(PropertiesConstants.REQUEST_TIMEOUT_KEY);
                
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost(RABBITMQ_HOST_RECEPTION);
                factory.setUsername(RABBITMQ_UID_RECEPTION);
                factory.setPassword(RABBITMQ_PWD_RECEPTION);

//                factory.setRequestedHeartbeat(Integer.valueOf(REQUEST_TIMEOUT));
                connection = factory.newConnection();
                channel = this.connection.createChannel();
                
//                channel.exchangeDeclare(RABBITMQ_QNAME_RECEPTION, RABBITMQ_QNAME_RECEPTION);
//                String queueName = channel.queueDeclare().getQueue();
//                channel.queueBind(queueName, RABBITMQ_QNAME_RECEPTION, "");
                channel.queueDeclarePassive(RABBITMQ_QNAME_RECEPTION);
                //channel.basicQos(1);                
                success = true;
            } catch (Exception e) {
                exception = true;
                e.printStackTrace();
                System.out.println("Error");
                
            }
            
            if(channel != null) {
              if (!exception) success = true;
              break;              
            }
        }
        
        return success;
    }
    
    public String getRABBITMQ_QNAME_RECEPTION(){
        return RABBITMQ_QNAME_RECEPTION;
    }
}

*/