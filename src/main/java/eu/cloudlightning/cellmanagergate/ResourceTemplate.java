package eu.cloudlightning.cellmanagergate;

import java.io.InputStream;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.cloudlightning.bcri.blueprint.BlueprintResourceTemplate;
import eu.cloudlightning.bcri.event.EventBPResourceTemplate;
import eu.cloudlightning.requestchecker.GWRequestParser;
import eu.cloudlightning.util.QueueMessageClient;
import eu.cloudlightning.util.QueueMessageForwarder;

public class ResourceTemplate implements Runnable {
    
	ControlRequest api_server = null;
    final AsyncResponse response;
    InputStream incomingData;
    String COMPONENTID;
    QueueMessageForwarder forwarder;
    QueueMessageClient listener;
    
    public ResourceTemplate(final AsyncResponse response, InputStream incomingData, ControlRequest api_server, 
    		String id, QueueMessageForwarder forwarder)
    {
        this.api_server = api_server;
        this.response = response;
        this.incomingData = incomingData;  
        this.COMPONENTID = id;
        this.forwarder = forwarder;
    }
    
    @Override
    public void run() {

    	String failureStr = "Mapping the input";
        EventBPResourceTemplate eventRT = null;
        try {

            // 1. create class objects with Gson
            ObjectMapper mapper = new ObjectMapper();
            //mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            BlueprintResourceTemplate resource = mapper.readValue(incomingData, BlueprintResourceTemplate.class);
            failureStr = "Translating from resource template to event ..";
            
            GWRequestParser checker = new GWRequestParser();
            
            eventRT = checker.parseResourceTemplate(resource, COMPONENTID);
            
            if (eventRT != null){
                failureStr = "Forwarding resource template event";
                // Send the CL-Resource to cell manager
                Gson gson = new GsonBuilder().create();
                String payloadStr = gson.toJson(eventRT);
                forwarder.sendMetrics(payloadStr);     
                response.resume(Response.status(200).entity("").build());
              
            }
            else { // Error
                failureStr = "Unexpected error at the creation of the event from the resource template";
                response.resume(Response.status(204).entity("").build());
            }

        } catch (Exception e) {
            System.out.println("Cell Manager REST server - Error Building the event from a resource template: " + failureStr + "\nMessage:"+ e.getMessage());
            if (failureStr.equals("Forwarding resource template event")){
            	response.resume(Response.status(409).entity("").build());
            }else{
            	response.resume(Response.status(400).entity("").build());
            }  
        }
    }

}