/*
 * Restful server to collect data from the plug and play interfaces. It provides to methods within the API, @POST, and @DELETE
 */
package eu.cloudlightning.cellmanagergate;

import eu.cloudlightning.properties.PropertiesConstants;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

@Path("/")
/**
 *
 * @author gabriel
 */
public class ControlRequest extends PropertiesConstants {

    private static Properties config;
    private static String COMPONENTID;
    private static eu.cloudlightning.util.QueueMessageForwarder forwarder;  
        
    static {
        config = new Properties();
        try {
          InputStream stream = new FileInputStream(PropertiesConstants.CONFIGURATION);
          try {
            config.load(stream);
            
            COMPONENTID = config.getProperty(PropertiesConstants.COMPONENTID_KEY);
            
            // configuration of the rabbitMQ parameters             
            forwarder = new eu.cloudlightning.util.QueueMessageForwarder(config);
            
            config.getProperty(PropertiesConstants.REQUEST_TIMEOUT_KEY);
          }
          finally {
            stream.close();
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
          throw new RuntimeException("Could not init class.", ex);
        }
    }
            

    @POST    
    @Path("/resource_template")
    @Consumes(MediaType.APPLICATION_JSON)
    public void registerResource(@Suspended final AsyncResponse response, InputStream incomingData) {
    	ResourceTemplate thr = new ResourceTemplate(response, incomingData, this, COMPONENTID, forwarder);
    	thr.run();              
    }
    
    @POST    
    @Path("/resource_decommission")
    @Consumes(MediaType.APPLICATION_JSON)
    public void resourceDecomission(@Suspended final AsyncResponse response, InputStream incomingData) {
    	ResourceDecommision thr = new ResourceDecommision(response, incomingData, this, COMPONENTID, forwarder);
    	thr.run();      
    }
    
}