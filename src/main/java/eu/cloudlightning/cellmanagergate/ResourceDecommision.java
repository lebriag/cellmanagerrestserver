package eu.cloudlightning.cellmanagergate;

import java.io.InputStream;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.cloudlightning.bcri.blueprint.BlueprintResourceDecommissionTemplate;
import eu.cloudlightning.bcri.event.EventResourceDecommission;
import eu.cloudlightning.requestchecker.GWRequestParser;
import eu.cloudlightning.util.QueueMessageClient;
import eu.cloudlightning.util.QueueMessageForwarder;

public class ResourceDecommision implements Runnable {
    
	ControlRequest api_server = null;
    final AsyncResponse response;
    InputStream incomingData;
    String COMPONENTID;
    QueueMessageForwarder forwarder;
    QueueMessageClient listener;
    
    public ResourceDecommision(final AsyncResponse response, InputStream incomingData, ControlRequest api_server, 
    		String id, QueueMessageForwarder forwarder)
    {
        this.api_server = api_server;
        this.response = response;
        this.incomingData = incomingData;  
        this.COMPONENTID = id;
        this.forwarder = forwarder;
    }
    
    @Override
    public void run() {

    	String failureStr = "Mapping the input";
      EventResourceDecommission eventRD = null;
      try {

          // 1. create class objects with Gson
          ObjectMapper mapper = new ObjectMapper();
          //mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
          BlueprintResourceDecommissionTemplate resource = mapper.readValue(incomingData, BlueprintResourceDecommissionTemplate.class);
          failureStr = "Translating from resource template to event ..";

          GWRequestParser checker = new GWRequestParser();
          
          eventRD = checker.parseResourceDecommission(resource, COMPONENTID);
          
          if (eventRD != null){
              failureStr = "Forwarding resource template event";
              // Send the CL-Resource to cell manager
              Gson gson = new GsonBuilder().create();
              String payloadStr = gson.toJson(eventRD);
              forwarder.sendMetrics(payloadStr);
              response.resume(Response.status(200).entity("").build());
            
          }
          else { // Error
              failureStr = "Unexpected error at the creation of the event from the resource decommission";
              response.resume(Response.status(409).entity("").build());
          }

      } catch (Exception e) {
          System.out.println("Cell Manager REST server - Error Building the event from a resource decommission: " + failureStr + "\nMessage:"+ e.getMessage());
          response.resume(Response.status(404).entity("").build());
      }
  }

}